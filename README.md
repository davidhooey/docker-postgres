# Dockerized Postgres

Run a Postgres and pgAdmin server.

## Configuration

- The `docker-compose.yml` file contains two services/containers, one each for pgAdmin and Postgres.
- The Postgres port of 5432 is exposed to the host machine. A local pgAdmin tool running on the host can connect directly to the Postgres container using `localhost:5432`.
- The pgAdmin port of 5050 is exposed to the host machine allowing access at [http://localhost:5050](http://localhost:5050).
- Postgres data and configuration files are stored in a Docker volume and will persist the data.
- The Docker network will have two servers with hostnames of `db` and `pgadmin`.
- Login to Postgres with username `postgres` and password `postgres`.
- Login to pgAdmin with username `pgadmin4@pgadmin.org` and password `admin`.
- A local `pg_hba.conf` file is added to the Postgres container to allow for all remote access.

## Running

Start both Postgres and pgAdmin containers.
```
docker-compose up
```

## Stopping

Stop both Postgres and pgAdmin containers.
```
docker-compose down
```

## psql

Ensure the Postgres container is running.
```
docker-compose exec db psql -U postgres
```

## bash

Ensure the Postgres container is running.
```
docker-compose exec db bash
```
